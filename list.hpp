///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 2.0
///
/// Custom made list
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 08a - Cat Wramg;er - EE 205 - Spr 2021
/// @date   08_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

#include "node.hpp"

//static unsigned int counter = 0;

class DoubleLinkedList {

public:
   //function members
   const bool empty() const;
   void push_front( Node* newNode );
   Node* pop_front();
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
   unsigned int size() const;
   //dll function members
   void push_back( Node* newNode );
   Node* pop_back();
   Node* get_last() const;
   Node* get_prev( const Node* currentNode ) const;
   void swap(Node* node1, Node* node2);
   const bool isSorted() const;
   void insertionSort();
   //additional functions
   const bool isIn( Node* aNode ) const;
   //ec functions
   void insert_after( Node* currentNode, Node* newNode );
   void insert_before( Node* currentNode, Node* newNode );

protected:
   Node* head = nullptr;
   Node* tail = nullptr;

};

