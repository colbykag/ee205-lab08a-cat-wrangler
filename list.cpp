///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 2.0
///
/// Custom made list
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   08_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cassert>

#include "list.hpp"

using namespace std;

static unsigned int counter = 0;

   const bool DoubleLinkedList::empty() const {
      bool isEmpty = false;
      if(head == nullptr)
      {
         isEmpty = true;
      }
      return isEmpty;
   }

   void DoubleLinkedList::push_front( Node* newNode ) { 
      if( newNode == nullptr)
         return;
      
      if(head != nullptr)//general case
      {
         newNode->next = head;
         newNode->prev = nullptr;
         head->prev = newNode;
         head = newNode;
      }
      else //if list is empty beforehand
      {
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      counter++;
   }

   Node* DoubleLinkedList::pop_front() {
      if(head == nullptr)//empty list
         return nullptr;
     
      if(head == tail){//one node in list
         Node* del = head;
         head = nullptr;
         tail = nullptr;
         counter--;
         return del;

      }
      else{
         Node* del = head;
         head = head->next;
         head->prev = nullptr;
         counter--;
         return del;
      }
   }

   Node* DoubleLinkedList::get_first() const {
      //Node* first = head;
      return head;
   }

   Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
      //Node* nextNode = currentNode->next;
      return currentNode->next;
   }

   unsigned int DoubleLinkedList::size() const {
      return counter;
   }

//new functions for dll
   void DoubleLinkedList::push_back( Node* newNode ) {
      if( newNode == nullptr)
         return;
      
      if(tail != nullptr)//general case
      {
         newNode->next = nullptr;
         newNode->prev = tail;
         tail->next = newNode;
         tail = newNode;
      }
      else //if list is empty beforehand
      {
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      counter++;

   }

   Node* DoubleLinkedList::pop_back() {
      if(tail == nullptr)//empty list
         return nullptr;
      
      if(tail == head){//only one node in list
         Node* del = tail;
         tail = nullptr;
         head = nullptr;
         counter--;
         return del;
      }
      else{
         Node* del = tail;
         tail = tail->prev;
         tail->next = nullptr;
         counter--;
         return del;
      }
   }

   Node* DoubleLinkedList::get_last() const {
     // Node* first = head;
      return tail;
   }

   Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
      //Node* prevNode = currentNode->prev;
      return currentNode->prev;
   }

//ec
   void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode) {
      //if newNode is a nullptr
      if(newNode == nullptr){
         return;
      }

      //if there is a currentNode but no head
      if(currentNode != nullptr && head == nullptr){
         assert( false );
      }
      //if there is a head but currentNode is a nullptr
      if(currentNode == nullptr && head != nullptr){
         assert( false );
      }

      if(head == nullptr && currentNode == nullptr){//if list is empty
         push_front( newNode );
      }
      else if(currentNode == tail){ //if currentNode is the last in the list
         push_back( newNode );
      }
      else{//general case
         Node* ogNext = currentNode->next;
         currentNode->next = newNode;
         newNode->prev = currentNode;
         newNode->next = ogNext;
         ogNext->prev = newNode;
         counter++;
      }
   }

   void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode) {
      //if new node is a nullptr
      if(newNode == nullptr)
         return;

      //if there is a currentNode but no head or tail
      if( (currentNode != nullptr && head == nullptr) || (currentNode != nullptr && tail == nullptr) ){
         assert( false );
      }
      //if there is a head or tail but currentNode is a nullptr
      if( (currentNode == nullptr && head != nullptr) || (currentNode == nullptr && tail != nullptr) ){
         assert( false );
      }

      if(head == nullptr || currentNode == head){//if list is empty or if the currentNode is first in the list
         push_front( newNode );
      }
      /*else if(currentNode == head){ //if currentNode is the first in the list
         newNode->next = currentNode;
         newNode->prev = nullptr;
         currentNode->prev = newNode;
         head = newNode;
      }*/
      else{//general case
         Node* ogPrev = currentNode->prev;
         currentNode->prev = newNode;
         newNode->next = currentNode;
         newNode->prev = ogPrev;
         ogPrev->next = newNode;
         counter++;
      }
 
   }

   const bool DoubleLinkedList::isIn( Node* aNode ) const{
      Node* currentNode = head;
      while( currentNode != nullptr ) {
         if(aNode == currentNode)
            return true;
         currentNode = currentNode->next;
      }
      return false;//if aNode is not in list
   }

   void DoubleLinkedList::swap( Node* node1, Node* node2) {
      //check if list is empty
      if(head == nullptr){
         cout << "unable to swap: list is empty" << endl;
         return;
      }

      //check if Node1 or Node2 are nullptrs
      if(node1 == nullptr || node2 == nullptr)
         return;

      //check if Node1 or Node2 are in list
      if(!isIn(node1) || !isIn(node2))
         return;

      //check if Node1 = Node2 and do nothing
      if(node1 == node2)
         return;

      //check if Node1 and Node2 are head and tail
      if((node1 == head && node2 == tail) || (node2 == head && node1 == tail)){
         //Node1 is original head and Node2 is original tail
         if(node1 == head){
            Node* next1 = node1->next;
            Node* prev2 = node2->prev;
            next1->prev = node2;
            prev2->next = node1;

            node1->prev = node2->prev;
            node2->next = node1->next;
            node1->next = nullptr;
            node2->prev = nullptr;
            tail = node1;
            head = node2;
         }
         //Node2 is original head and Node1 is original tail
         else{
            Node* next2 = node2->next;
            Node* prev1 = node1->prev;
            next2->prev = node1;
            prev1->next = node2;

            node2->prev = node1->prev;
            node1->next = node2->next;
            node2->next = nullptr;
            node1->prev = nullptr;
            tail = node2;
            head = node1;
         }
      }
      else if(node1 == head || node2 == head){//check to see if Node1 or Node2 is head
         if(node1 == head) {//Node1 is original head
            Node* next1 = node1->next;
            Node* next2 = node2->next;
            Node* prev2 = node2->prev;
            next1->prev = node2;
            next2->prev = node1;
            prev2->next = node1;

            node1->prev = node2->prev;
            Node* placeHolder = node2->next;
            node2->next = node1->next;
            node1->next = placeHolder;
            node2->prev = nullptr;
            head = node2;
         }
         else{//Node2 is original head
            Node* next1 = node1->next;
            Node* next2 = node2->next;
            Node* prev1 = node1->prev;
            next1->prev = node2;
            next2->prev = node1;
            prev1->next = node2;

            node2->prev = node1->prev;
            Node* placeHolder = node1->next;
            node1->next = node2->next;
            node2->next = placeHolder;
            node1->prev = nullptr;
            head = node1;
         }
      }
      else if(node1 == tail || node2 == tail){//check to see if Node1 or Node2 is tail
         if(node1 == tail) {//Node1 is original tail
            Node* next2 = node2->next;
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            next2->prev = node1;
            prev1->next = node2;
            prev2->next = node1;


            node1->next = node2->next;
            Node* placeHolder = node2->prev;
            node2->prev = node1->prev;
            node1->prev = placeHolder;
            node2->next = nullptr;
            tail = node2;
         }
         else{//Node2 is original tail
            Node* next1 = node1->next;
            Node* prev1 = node1->prev;
            Node* prev2 = node2->prev;
            next1->prev = node2;
            prev1->next = node2;
            prev2->next = node1;


            node2->next = node1->next;
            Node* placeHolder = node1->prev;
            node1->prev = node2->prev;
            node2->prev = placeHolder;
            node1->next = nullptr;
            tail = node1;
         }
      }
      else{//general case (Neither Node is head or tail)
         Node* next1 = node1->next;
         Node* next2 = node2->next;
         Node* prev1 = node1->prev;
         Node* prev2 = node2->prev;
         next1->prev = node2;
         next2->prev = node1;
         prev1->next = node2;
         prev2->next = node1;

         Node* placeHolder = node1->prev;
         node1->prev = node2->prev;
         node2->prev = placeHolder;
         placeHolder = node1->next;
         node1->next = node2->next;
         node2->next = placeHolder;
      }

   }

   const bool DoubleLinkedList::isSorted() const {
      if(counter <= 1)
         return true;

      for(Node* i = head; i->next != nullptr; i = i->next) {
         if(*i > *i->next)
            return false;
      }
      return true;
   }

   void DoubleLinkedList::insertionSort() {
      if(counter <= 1)//if list is empty or has 1 item
         return;

      for(Node* i = head; i->next != nullptr; i = i->next ) {//outer loop
         Node* minNode = i;

         for(Node*j = i->next; j != nullptr; j = j->next ) {//inner loop
            if( *minNode > *j )
               minNode = j;
         }

         swap( i, minNode );
         i = minNode;
      }
   }
