///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file main.cpp
/// @version 1.0
///
/// The main loop for this lab
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

// The purpose of this main() function is to exercise your
// DoubleLinkedList and quickly visualize the correct operation
// of the methods.

int main() {
   Node node;
//   DoubleLinkedList list;
   cout << "Welcome to Cat Wrangler" << endl;

	Cat::initNames();

	Cat* newCat = Cat::makeCat();

	cout << newCat->getInfo() << endl << endl;


	DoubleLinkedList list = DoubleLinkedList();

	// Put 16 cats in my list
	for( int i = 0 ; i < 8 ; i++ ) {
		list.push_front( Cat::makeCat() );
		//list.push_back( Cat::makeCat() );
      //check to see if adding to right place
      cout << "first cat" << endl;
      cout << ((Cat*)list.get_first())->getInfo() << endl;
      cout << "last cat" << endl;
      cout << ((Cat*)list.get_last())->getInfo() << endl << endl;
	}

   cout << "Removal" << endl << endl;

	// Remove 7 cats in my list
   // changed to 6 to test insert_after
	for( int i = 0 ; i < 6 ; i++ ) {
		//list.pop_front();
		list.pop_back();
      //check to see if adding to right place
      cout << "first cat" << endl;
      cout << ((Cat*)list.get_first())->getInfo() << endl;
      cout << "last cat" << endl;
      cout << ((Cat*)list.get_last())->getInfo() << endl << endl;
	}

   //test swapping head and tail w/ only 2 cats
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl << endl;
   list.swap(list.get_first(),list.get_last());
   cout << "after swap" << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl << endl;

   //testing insert_after
   cout << endl << "testing insert after" << endl << endl;
   Node* SecondCat = list.get_last();
   for( int i = 0; i < 3; i++) {
      list.insert_after( SecondCat, Cat::makeCat() );
      cout << "current cat" << endl;
      cout << ((Cat*)SecondCat)->getInfo() << endl;
      cout << "next cat" << endl;
      cout << ((Cat*)list.get_next(SecondCat))->getInfo() << endl;
      cout << "last cat" << endl;
      cout << ((Cat*)list.get_last())->getInfo() << endl << endl;
      cout << list.size() << endl;
   }

   //testing insert_before
   /*cout << endl << "testing insert before" << endl << endl;
   Node* ogFirstCat = list.get_first();
   for( int i = 0; i < 3; i++) {
      list.insert_before( ogFirstCat, Cat::makeCat() );
      cout << "current cat" << endl;
      cout << ((Cat*)ogFirstCat)->getInfo() << endl;
      cout << "prev cat" << endl;
      cout << ((Cat*)list.get_prev(ogFirstCat))->getInfo() << endl;
      cout << "first cat" << endl;
      cout << ((Cat*)list.get_first())->getInfo() << endl << endl;
      cout << list.size() << endl;
   }*/

   //testing swap of head and tail
   cout << endl << "testing swap of head and tail" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   cout << "second to last cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   list.swap(list.get_first(), list.get_last());
   cout << endl << "after swap" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   cout << "second to last cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;

   //testing swap of head and tail
   cout << endl << "testing swap of head and tail" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   cout << "second to last cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   list.swap(list.get_first(), list.get_last());
   cout << endl << "after swap" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "last cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   cout << "second to last cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;

   //testing swap of head and other node  
/*   cout << endl << "testing swap of head and other node" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_first(), list.get_next(list.get_first()));
   cout << endl << "after swapping head and 2nd cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_prev(list.get_last())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;

   //testing swap of head and other node  
   cout << endl << "testing swap of head and other node" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_prev(list.get_prev(list.get_last())),list.get_first());
   cout << endl << "after swapping head and 3rd cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_prev(list.get_last())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
*/
   //testing swap of tail and other node  
/*   cout << endl << "testing swap of tail and other node" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_prev(list.get_last()),list.get_last());
   cout << endl << "after swapping last and 4th cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;

   //testing swap of tail and other node  
   cout << endl << "testing swap of tail and other node" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_last(), list.get_prev(list.get_prev(list.get_last())));
   cout << endl << "after swapping last and 3rd cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
*/
   //testing swap of 2 random nodes
/*   cout << endl << "testing swap of 2 random nodes" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_next(list.get_first()),list.get_prev(list.get_last()));
   cout << endl << "after swapping 4th and 2nd cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;

   //testing swap of 2 random nodes
   cout << endl << "testing swap of 2 random nodes" << endl << endl;
   cout << "list size: " << list.size() << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_next(list.get_next(list.get_first())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
   list.swap(list.get_next(list.get_first()),list.get_prev(list.get_prev(list.get_last())));
   cout << endl << "after swapping 3rd and 2nd cat" << endl << endl;
   cout << "first cat" << endl;
   cout << ((Cat*)list.get_first())->getInfo() << endl;
   cout << "second cat" << endl;
   cout << ((Cat*)list.get_next(list.get_first()))->getInfo() << endl;
   cout << "third cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_prev(list.get_last())))->getInfo() << endl;
   cout << "fourth cat" << endl;
   cout << ((Cat*)list.get_prev(list.get_last()))->getInfo() << endl;
   cout << "last (5th) cat" << endl;
   cout << ((Cat*)list.get_last())->getInfo() << endl;
*/
/*
	list.insertionSort();  // They should be sorted by name

	// Swap the first 2 and the last 2 cats (using 2 different techniques)
	Cat* cat1 = (Cat*)list.pop_front();
	Cat* cat2 = (Cat*)list.pop_back();

	list.swap( list.get_first(), list.get_last() );

	list.push_back( cat1 );
	list.push_front( cat2 );

	// Print the list in reverse order:
	//   - You'll see the first 2 cats in alphabetical order
	//   - The bulk of the cats in reverse order
	//   - The last 2 cats in alphabetic order
	for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
		cout << cat->getInfo() << endl;
	}
*/
}

