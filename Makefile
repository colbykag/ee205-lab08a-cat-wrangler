###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author Colby Kagamida <colbykag@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   @todo 08_Apr_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main mytest queueSim

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

mytest.o: mytest.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o: node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o: list.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

main: *.hpp main.o cat.o node.o list.o
	$(CXX) -o $@ main.o cat.o node.o list.o

mytest: *.hpp mytest.o cat.o node.o list.o
	$(CXX) -o $@ mytest.o cat.o node.o list.o

queueSim.o: queueSim.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

queueSim: *.hpp queueSim.o node.o list.o
	$(CXX) -o $@ queueSim.o node.o list.o

clean:
	rm -f *.o main queueSim mytest burger.txt taco.txt chip.txt
